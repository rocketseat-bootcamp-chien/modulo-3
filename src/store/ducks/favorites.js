/*
 *  Types
 */

export const Types = {
  ADD_REQUEST: 'favorite/ADD_REQUEST',
  ADD_SUCCESS: 'favorite/ADD_SUCCESS',
  ADD_FAILURE: 'favorite/ADD_FAILURE'
};

/*
 *  Reducer
 */
const INITIAL_STATE = {
  error: null,
  loading: false,
  data: []
};

export default function favorites(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.ADD_REQUEST:
      return { ...state, loading: true };
    case Types.ADD_SUCCESS:
      return { ...state, loading: false, error: null, data: [...state.data, action.payload.data] };
    case Types.ADD_FAILURE:
      return { ...state, loading: false, error: action.payload.error };
    default:
      return state;
  }
}

/*
 *  Actions
 */

export const Creators = {
  addFavoriteRequest: repository => ({
    type: Types.ADD_REQUEST,
    payload: { repository }
  }),

  addFavoriteSuccess: data => ({
    type: Types.ADD_SUCCESS,
    payload: { data }
  }),

  addFavoriteFailure: error => ({
    type: Types.ADD_FAILURE,
    payload: { error }
  })
};
